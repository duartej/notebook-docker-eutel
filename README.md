# notebook-docker-eutel

Gather some notebooks with instruccions related with the eutelescope software installation, used for the test-beam analysis
This includes notes about the virtual machine and docker installation, manage and use.

## Notebook visualization
As Bitbucket is still not supporting the notebook renderer [nbviewer](http://nbviewer.jupyter.org/) as github does,
you can launch your own jupyter notebook instance
```bash
$ jupyter notebook
```
and load the relevant notebooks. 
As soon as the renderer is available, this section will contain the links to the notebooks using the nbviewer.



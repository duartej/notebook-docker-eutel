#!/bin/bash

# EUDAQ integration docker image setup
# Run it first time to create all the needed
# infrastructure,

VERSION="0.1-alpha"

# 1. Check it is running as regular user
if [ "${EUID}" -eq 0 ];
then
    echo "Not run this as root"
    exit -2
fi

# 2. Check if the setup was run before:
#    it exist in this directory:
#     - Dockerfile
#     - docker-compose.xml
#    and 
#     - $HOME/eudaq-data/data
#     - $HOME/eudaq-data/logs
if [ -e ".setupdone" ];
then
    cat .setupdone
    exit -3
fi

DOCKERDIR=${PWD}

# 3. Download the code: XXX This can be done in the image actually
CODEDIR=${HOME}/repos/eudaq
mkdir -p ${CODEDIR} && cd ${CODEDIR}/.. ;
if [ "X$(command -v git)" == "X" ];
then
    echo "You will need to install git (https://git-scm.com/)"
    exit -1;
fi

echo "Cloning EUDAQ into : $(pwd)"
git clone  https://github.com/eudaq/eudaq.git eudaq

if [ "$?" -eq 128 ];
then
    echo "Repository already available at '${CODEDIR}'"
    echo "Remove it if you want to re-clone it"
else
    # Change to the v1.7
    #echo "Switch to v1.7.0"
    #git checkout tags/v1.7.0 -b v1.7.0
    # Better to switch to the 1.7-dev (contains bug fixes)
    echo "Switch to v1.7-dev"
    git checkout v1.7-dev
    echo "======================================================="
    echo "If this is for developing purposes, it is convenient to"
    echo "fork the https://github.com/eudaq/eudaq.git into your  "
    echo "github account and perform regular updates:"
    echo "1. Define your github forked repository as origin"
    echo "2. Define the original eudaq repository as upstream"
    echo "3. Perform regularly to keep your repo updated:"
    echo "4.   git pull upstream" 
    echo "======================================================="
    # Download the TLU needed files 
    echo "Downloading some external files for the TLU, introduce"
    echo "your LXPLUS user: "
    read LXPLUSUSER;
    echo "Connecting to LXPLUS with the user: '${LXPLUSUSER}'..."
    scp -r ${LXPLUSUSER}@lxplus.cern.ch:/afs/desy.de/group/telescopes/tlu/ZestSC1 ${CODEDIR}/extern
    scp -r ${LXPLUSUSER}@lxplus.cern.ch:/afs/desy.de/group/telescopes/tlu/tlufirmware ${CODEDIR}/extern
fi

# 3. Fill the place-holders of the .templ-Dockerfile 
#    and .templ-docker-compose.yml and create the needed
#    directories
DATADIR=${HOME}/eudaq_data/data
LOGSDIR=${HOME}/eudaq_data/logs
mkdir -p $DATADIR
mkdir -p $LOGSDIR
cd ${DOCKERDIR}
# -- copying relevant files
cp .templ-Dockerfile Dockerfile
sed -i "s/@VERSION/${VERSION}/g" Dockerfile
cp .templ-Dockerfile-bdaq53 Dockerfile-bdaq53
sed -i "s/@VERSION/${VERSION}/g" Dockerfile-bdaq53
cp .templ-docker-compose.yml docker-compose.yml
sed -i "s#@CODEDIR#${CODEDIR}#g" docker-compose.yml
sed -i "s#@DATADIR#${DATADIR}#g" docker-compose.yml
sed -i "s#@LOGSDIR#${LOGSDIR}#g" docker-compose.yml
cp .templ-docker-compose-tlu.yml docker-compose-tlu.yml
sed -i "s#@CODEDIR#${CODEDIR}#g" docker-compose-tlu.yml
sed -i "s#@DATADIR#${DATADIR}#g" docker-compose-tlu.yml
sed -i "s#@LOGSDIR#${LOGSDIR}#g" docker-compose-tlu.yml


# 4. Create a .setupdone file with some info about the
#    setup
cat << EOF > .setupdone
EUDAQ integration docker image and services
-------------------------------------------
Last setup performed at $(date)
DATA DIRECTORY: ${DATADIR}
LOGS DIRECTORY: ${LOGSDIR}
CODE DIRECTORY: ${CODEDIR}
EOF
cat .setupdone


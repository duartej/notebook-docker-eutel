FROM eudaqv1-ubuntu:latest
LABEL author="jorge.duarte.campderros@cern.ch" \ 
    version="@VERSION" \ 
    description="Docker image to integrate the RD53A chip \
    using the bdaq53 readout system into EUDAQ"

# Be sure running as root
USER 0

# Place at the directory
WORKDIR /bdaq53

# Install all dependencies
RUN apt-get update \
  && apt-get -y install \ 
      python-pip \ 
      python3-pip \ 
      python-yaml \ 
      python-requests \
      python-pyqt5 \
      python-tk \ 
  && rm -rf /var/lib/apt/lists/*

# Download minicoda (and recovering permissions)
RUN mkdir -p /bdaq53 \ 
    && wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /bdaq53/miniconda3.sh \
    && wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O /bdaq53/miniconda2.sh \
    && chown -R eudaquser:eudaquser /bdaq53 \  
    && chown -R eudaquser:eudaquser /eudaq 

# Change to user and conda installation
USER eudaquser
ENV HOME="/eudaq"
ENV PATH="${PATH}:${HOME}/.local/bin:/bdaq53/miniconda/bin"
ENV PYTHONPATH="${HOME}/.local/lib:${PYTHONPATH}"
RUN cd /bdaq53 \ 
    && /bin/bash miniconda2.sh -b -p /bdaq53/miniconda \ 
    && /bin/bash -c "source activate" \ 
    && conda update -y -n base conda \
    && conda install -y \ 
       numpy \ 
       bitarray \ 
       pyyaml \ 
       scipy \ 
       numba \ 
       pytables \ 
       matplotlib \ 
       tqdm \ 
       pyzmq \ 
       blosc \ 
       psutil \ 
    && pip install --upgrade pip
RUN pip install --user git+https://gitlab.cern.ch/silab/bdaq53.git@eudaq

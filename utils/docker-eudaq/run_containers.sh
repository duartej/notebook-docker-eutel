#!/bin/bash 

# The container creation activating the X-server
# Be sure we have the proper rights for docker
xhost +local:docker
# Creation of the container (and let the control there)
docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY --rm eudaqv1-ubuntu:latest
